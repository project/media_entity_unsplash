<?php

namespace Drupal\media_entity_unsplash;

/**
 * Functions that can be used for the Unsplash API.
 */
interface UnsplashFetcherInterface {

  /**
   * Retrieves a image from Unsplash by its ID.
   *
   * @param int $url
   *   The image ID.
   *
   * @return array
   *   The tweet information.
   */
  public function fetchImage($url);

  /**
   * Returns the current Unsplash API credentials.
   *
   * @return array
   *   The API credentials. This will be an array containing the
   *   application_id and the utm_source.
   */
  public function getCredentials();

  /**
   * Sets the credentials for accessing Unsplash API.
   *
   * @param string $access_key
   *   The consumer key.
   * @param string $secret_key
   *   The utm source.
   */
  public function setCredentials($access_key, $secret_key);

}
