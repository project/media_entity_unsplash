<?php

namespace Drupal\media_entity_unsplash\Plugin\media\Source;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\media_entity_unsplash\UnsplashFetcherInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Unsplash media source functionality.
 *
 * You can find possible values to use in the providers object in the list
 * here: https://oembed.com/providers.json.
 *
 * @MediaSource(
 *   id = "unsplash",
 *   label = @Translation("Unsplash"),
 *   description = @Translation("Embed Unsplash content."),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   forms = {
 *     "media_library_add" =
 *   "\Drupal\media_entity_unsplash\Form\UnsplashMediaLibraryAddForm",
 *   }
 * )
 */
class Unsplash extends MediaSourceBase {

  /**
   * Key for "description" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_DESCRIPTION = 'description';

  /**
   * Key for "Urls" ob metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_IMAGE = 'urls';

  /**
   * Key for "URL" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_ID = 'id';

  /**
   * Key for "Image" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_CREATED_AT = 'created_at';

  /**
   * API client for Unsplash service.
   *
   * @var \Drupal\media_entity_unsplash\UnsplashFetcherInterface
   */
  protected $unsplashFetcher;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger channel for media.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    UnsplashFetcherInterface $unsplash_fetcher,
    ClientInterface $http_client,
    FileSystemInterface $file_system,
    LoggerInterface $logger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $entity_field_manager,
      $field_type_manager,
      $config_factory
    );

    $this->unsplashFetcher = $unsplash_fetcher;
    $this->fileSystem = $file_system;
    $this->logger = $logger;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('media_entity_unsplash.unsplash_fetcher'),
      $container->get('http_client'),
      $container->get('file_system'),
      $container->get('logger.factory')->get('media_entity_unsplash')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'thumbnails_directory' => 'public://unsplash_thumbnails',
      'height' => '',
      'width' => '',
      'use_unsplash_api' => TRUE,
      'generate_thumbnails' => TRUE,
      'access_key' => '',
      'secret_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      static::METADATA_ATTRIBUTE_ID => $this->t('Image ID'),
      static::METADATA_ATTRIBUTE_DESCRIPTION => $this->t('Description'),
    ];
  }

  /**
   * Sets credentials passed in to the fetcher class.
   *
   * @param int $url
   *   The Unsplash URL.
   *
   * @return array
   *   Array with the fetched data.
   */
  public function fetchImageT($url) {
    $this->unsplashFetcher->setCredentials(
      $this->configuration['access_key'],
      $this->configuration['secret_key'],
    );
    return $this->unsplashFetcher->fetchImage($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $media_url = $this->getSourceFieldValue($media);

    // Return null if source field is empty.
    if (empty($media_url)) {
      return NULL;
    }

    // Check if there is any data to fetch.
    $data = $this->fetchImageT($media_url);
    if (!$data) {
      return NULL;
    }

    switch ($attribute_name) {
      // Default name is the description.
      case 'default_name':
      case static::METADATA_ATTRIBUTE_DESCRIPTION:
        return $data->{static::METADATA_ATTRIBUTE_DESCRIPTION};

      case static::METADATA_ATTRIBUTE_IMAGE:
        return $data->{static::METADATA_ATTRIBUTE_IMAGE};

      case 'thumbnail_uri':
        return $this->getLocalThumbnailUri($data);

      case static::METADATA_ATTRIBUTE_CREATED_AT:
        return $data->{static::METADATA_ATTRIBUTE_CREATED_AT};

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['generate_thumbnails'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Generate thumbnails'),
      '#default_value' => $this->configuration['generate_thumbnails'],
      '#description' => $this->t(
        'If checked, Drupal will automatically generate thumbnails from Unsplash provided images.'
      ),
    ];

    $form['use_unsplash_api'] = [
      '#type' => 'select',
      '#title' => $this->t(
        'Whether to use Unsplash api to fetch images or not.'
      ),
      '#description' => $this->t(
        "Use the Unsplash API to fetch images from Unsplash"
      ),
      '#default_value' => empty($this->configuration['use_unsplash_api']) ? 0 : $this->configuration['use_unsplash_api'],
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
    ];

    $form['access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access key'),
      '#description' => $this->t('Access key of the application that you registered at Unsplash'),
      '#default_value' => empty($this->configuration['access_key']) ? NULL : $this->configuration['access_key'],
      '#states' => [
        'visible' => [
          ':input[name="source_configuration[use_unsplash_api]"]' => ['value' => '1'],
        ],
      ],
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#description' => $this->t('Secret key of the application that you registered at Unsplash'),
      '#default_value' => empty($this->configuration['secret_key']) ? NULL : $this->configuration['secret_key'],
      '#states' => [
        'visible' => [
          ':input[name="source_configuration[use_unsplash_api]"]' => ['value' => '1'],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(
    MediaTypeInterface $type,
    EntityViewDisplayInterface $display
  ) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'media_entity_unsplash_unsplash_embed',
      'label' => 'visually_hidden',
    ]);
  }

  /**
   * Objects with the data for generating the thumbnail.
   *
   * @param object $data
   *   Data returned from Unsplash API.
   *
   * @return string|null
   *   Either the URL of a local thumbnail, or NULL.
   */
  protected function getLocalThumbnailUri($data) {
    $remote_thumbnail_url = $data->{self::METADATA_ATTRIBUTE_IMAGE}['full'];
    if (!$remote_thumbnail_url) {
      return NULL;
    }

    $directory = $this->configuration['thumbnails_directory'];
    $local_thumbnail_uri = "$directory/" . Crypt::hashBase64(
        $remote_thumbnail_url
      ) . '.' . pathinfo($remote_thumbnail_url, PATHINFO_EXTENSION);
    // This assumes they are all JPEG. Is that safe?
    $local_thumbnail_uri .= '.jpg';

    // If the local thumbnail exists, return URI.
    if (file_exists($local_thumbnail_uri)) {
      return $local_thumbnail_uri;
    }

    // The local thumbnail doesn't exist yet, so try to download it. First,
    // ensure that the destination directory is writable, and if it's not,
    // log an error and bail out.
    if (!$this->fileSystem->prepareDirectory(
      $directory,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    )) {
      $this->logger->warning(
        'Could not prepare thumbnail destination directory @dir for oEmbed media.',
        [
          '@dir' => $directory,
        ]
      );
      return NULL;
    }

    try {
      $response = $this->httpClient->get($remote_thumbnail_url);
      if ($response->getStatusCode() === 200) {
        $this->fileSystem->saveData(
          (string) $response->getBody(),
          $local_thumbnail_uri,
          FileSystemInterface::EXISTS_REPLACE
        );
        return $local_thumbnail_uri;
      }
    }
    catch (RequestException $e) {
      $this->logger->warning($e->getMessage());
    }
    catch (FileException $e) {
      $this->logger->warning(
        'Could not download remote thumbnail from {url}.',
        [
          'url' => $remote_thumbnail_url,
        ]
      );
    }
    return NULL;
  }

}
