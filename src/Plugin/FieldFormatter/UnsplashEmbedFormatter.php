<?php

namespace Drupal\media_entity_unsplash\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\media\Entity\MediaType;
use Drupal\media_entity_unsplash\Plugin\media\Source\Unsplash;

/**
 * Plugin implementation of the 'Songwhip embed' formatter.
 *
 * @FieldFormatter(
 *   id = "media_entity_unsplash_unsplash_embed",
 *   label = @Translation("Unsplash embed"),
 *   allow_field_types = {
 *     "string"
 *   }
 * )
 */
class UnsplashEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $media = $items->getEntity();
    $unsplash = $media->getSource();
    foreach ($items as $delta => $item) {
      $url = $unsplash->getMetadata($media, Unsplash::METADATA_ATTRIBUTE_IMAGE['full']);
      if ($url) {
        $element[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'src' => $url,
            'height' => '500px',
            'width' => '500px',
            'class' => ['media-oembed-content'],
          ],
        ];
      }
      else {
        $element[$delta] = [
          '#markup' => $item->value,
        ];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if ($field_definition->getTargetEntityTypeId() !== 'media') {
      return FALSE;
    }

    if (parent::isApplicable($field_definition)) {
      $media_type = $field_definition->getTargetBundle();

      if ($media_type) {
        $media_type = MediaType::load($media_type);
        return $media_type && $media_type->getSource() instanceof Unsplash;
      }
    }
    return FALSE;

  }

}
