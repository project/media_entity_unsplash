<?php

namespace Drupal\media_entity_unsplash;

use Drupal\Core\Cache\CacheBackendInterface;
use Unsplash\HttpClient;
use Unsplash\Photo;

/**
 * Fetch, and cache, data from the Unsplash API.
 */
class UnsplashFetcher implements UnsplashFetcherInterface {

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Set of Unsplash API credentials.
   *
   * @var array
   */
  protected $credentials = [];

  /**
   * Cache bin for storing fetched API responses.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache bin.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * The Unsplash image URL.
   *
   * @param string $url
   *   Unsplash URL.
   *
   * @return false|object
   *   A generic object containing the decoded JSON response, or false.
   *
   * @throws \Exception
   */
  public function fetchImage($url) {
    if ($this->cache && $cached_response = $this->cache->get($url)) {
      return $cached_response->data;
    }

    $path = parse_url($url);
    // Setting path of the parsed url as variable.
    $str = ($path['path']);
    // Trims /photos/ from the url to get let only the id.
    $exploded = explode('-', trim($str, "/photos/"));
    $id = end($exploded);
    ;

    // Query Unsplash API.
    $response = Photo::find($id);

    if (empty($response)) {
      throw new \Exception("Could not retrieve response for $url.");
    }

    // If we have a cache, store the response for future use.
    if ($this->cache) {
      // Tweets don't change often, so the response should expire from the cache
      // on its own in 90 days.
      $this->cache->set($id, $response, time() + (86400 * 90));
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials() {
    return $this->credentials;
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials($access_key, $secret_key) {
    $this->credentials = [
      'applicationId' => $access_key,
      'secret' => $secret_key,
      'utmSource' => 'media_entity_unsplash',
    ];

    // Landing on the page for the first time, set up a connection with private
    // application details registered with Unsplash and redirect user to
    // authenticate.
    if ($this->credentials) {
      HttpClient::init($this->credentials);
    }
  }

}
