# Media Entity: Unsplash

This module provides a Drupal media entity for adding photos
from [Unsplash.com](https://unsplash.com) to your website.

To get a specific photo, you will need to get the photo's ID
(e.g._2ptmnitpUcg_), which can be found in the URL of the photo on Unsplash's
website.

You will need an Unsplash account and register your application.

At the moment this module only allows using public Unsplash photo's.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_entity_unsplash).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/media_entity_unsplash).


## Requirements

* You will need an Unsplash account and you must register an application
* This module requires no modules outside of Drupal core


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

After installation an _Unsplash_ media type will be available in your website.


## Configuration

1. Go to Administration > Structure > Media types _(/admin/structure/media)_
1. Locate the _Unsplash_ media item
1. Choose _Edit_ from the dropdown for the _Unsplash_ media item
1. Enter the Access key and the Application name of the application
   that you registered at Unsplash


## Usage

To get a specific photo, you will need to get the photo's ID, which can be
found in the URL of the photo on Unsplash's website.

1. Choose a photo from Unsplash.com's website to use on your Drupal website
1. The last part of the URL is the ID of the photo. For example, in the
  URL _[https://unsplash.com/photos/2ptmnitpUcg](https://unsplash.com/photos/2ptmnitpUcg)_ the ID is _2ptmnitpUcg_. Copy this ID.
1. In your Drupal website got to _/media/add_, and choose _Unsplash_
1. Enter a name and an Unsplash ID. You may also enter the full URL
   to the image.
1. Save the item.

The photo is now available to be added as a media item in your website.


## Credits

Thanks to:

- [Ali el Mansouri](https://www.drupal.org/u/aelm) for creating the module.
- [Finalist](https://www.finalist.nl) for sponsoring the maintenance.
- Denise Jans for the [beautiful photo of Utrecht](https://unsplash.com/photos/2ptmnitpUcg).


## Disclaimer

Please note that the module is still a work in progress and may contain
some bugs.

At the moment this module only allows public Unsplash photo's.


## Maintainers

- Boris Doesborg - [batigolix](https://www.drupal.org/u/batigolix)
- Fabian de Rijk - [fabianderijk](https://www.drupal.org/u/fabianderijk)
- Ali el Mansouri - [Aelm](https://www.drupal.org/u/aelm)
- Wouter Immerzeel - [immoreel](https://www.drupal.org/u/immoreel)
- Tom van Vliet - [tomvv](https://www.drupal.org/u/tomvv)
